//
//  ChartAndSegmentedControlCell.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import Charts

class ChartCell: UITableViewCell {
    private var chartView: LineChartView!
    
    private var chartData = [Date: Double]()
    private var isEmptyLabel: UILabel!
    
    // MARK: - Setup Segmented control
    
    func setupChart(chartData: [Date: Double]) {
        self.chartData = chartData
        
        chartView?.removeFromSuperview()
        chartView = LineChartView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 100))
        chartView.clipsToBounds = false
        chartView.chartDescription?.text = ""
        chartView.legend.enabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        chartView.xAxis.labelPosition = .bottom
        
        let marker = ChartMarker(color: .white,
                                 font: .systemFont(ofSize: 16),
                                 textColor: .blue,
                                 insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        addSubview(chartView)
        
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        chartView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        chartView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        chartView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        chartView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true
        
        isEmptyLabel?.removeFromSuperview()
        isEmptyLabel = UILabel()
        isEmptyLabel.text = "Нет данных"
        isEmptyLabel.textColor = .blue
        isEmptyLabel.font = UIFont.boldSystemFont(ofSize: 18)
        isEmptyLabel.isHidden = true
        
        addSubview(isEmptyLabel)
        
        isEmptyLabel.translatesAutoresizingMaskIntoConstraints = false
        isEmptyLabel.centerXAnchor.constraint(equalTo: chartView.centerXAnchor).isActive = true
        isEmptyLabel.centerYAnchor.constraint(equalTo: chartView.centerYAnchor).isActive = true
        
        chartView.xAxis.valueFormatter = DateValueFormatter(type: .threeMounths)
        setChartData(values: chartData)
        
    }
    
    private func setChartData(values: [Date: Double]) {
        isEmptyLabel.isHidden = !values.isEmpty
        
        var keys = [Date]()
        for key in values.keys {
            keys.append(key)
        }
        keys = keys.sorted(by: { $0 < $1 })
        
        var chartListDataEntry = [ChartDataEntry]()
        for key in keys {
            chartListDataEntry.append(ChartDataEntry(x: key.timeIntervalSince1970, y: values[key]!))
        }

        let set = LineChartDataSet(values: chartListDataEntry, label: "")
        set.setColor(.blue)
        set.lineWidth = 4
        set.highlightColor = .clear
        set.drawCirclesEnabled = false
        set.drawValuesEnabled = false
        set.formLineWidth = 1
        set.formSize = 15
    
        chartView.data = LineChartData(dataSet: set)

        chartView.highlightValue(nil)
        
        chartView.animate(xAxisDuration: 0.5)
    }
}

//
//  ChartAndSegmentedControlCell.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import Charts

class ChartAndSegmentedControlCell: UITableViewCell {
    
    private var segmentedControl: SegmentedControl!
    private var chartView: LineChartView!
    private var isEmptyLabel: UILabel!
    
    enum TypePeriod {
        case today, week, month, threeMounths, sixMounths, year, allPeriod
    }
    
    private var segmentedControlTitles = [String]()
    private var segmentedControlData = [TypePeriod]() {
        didSet {
            segmentedControlTitles.removeAll()
            for item in segmentedControlData {
                switch item {
                case .today:
                    segmentedControlTitles.append("Сегодня")
                case .week:
                    segmentedControlTitles.append("Неделя")
                case .month:
                    segmentedControlTitles.append("Месяц")
                case .threeMounths:
                    segmentedControlTitles.append("3 Месяца")
                case .sixMounths:
                    segmentedControlTitles.append("6 Месяцев")
                case .year:
                    segmentedControlTitles.append("Год")
                case .allPeriod:
                    segmentedControlTitles.append("Все время")
                }
            }
        }
    }
    private var chartData = [[[Date: Double]]]()
    
    // MARK: - Setup Segmented control
    
    func setupSegmentedControlAndChart(segmentedControlData: [TypePeriod], chartData: [[[Date: Double]]]) {
        self.segmentedControlData = segmentedControlData
        self.chartData = chartData
        
        segmentedControl?.removeFromSuperview()
        segmentedControl = SegmentedControl(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 44))
        segmentedControl.addTarget(self, action: #selector(didChangeValue(_:)), for: .valueChanged)
        
        segmentedControl.items = segmentedControlTitles
        addSubview(segmentedControl)
        
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        segmentedControl.heightAnchor.constraint(equalToConstant: 44).isActive = true
        segmentedControl.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        segmentedControl.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        segmentedControl.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        
        chartView?.removeFromSuperview()
        chartView = LineChartView(frame: CGRect(x: 0, y: 44, width: UIScreen.main.bounds.width, height: 100))
        chartView.clipsToBounds = false
        chartView.chartDescription?.text = ""
        chartView.legend.enabled = false
        chartView.rightAxis.drawLabelsEnabled = false
        chartView.xAxis.labelPosition = .bottom
        
        let marker = ChartMarker(color: .white,
                                 font: .systemFont(ofSize: 16),
                                 textColor: .blue,
                                 insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8))
        marker.chartView = chartView
        marker.minimumSize = CGSize(width: 80, height: 40)
        chartView.marker = marker
        
        addSubview(chartView)
        
        chartView.translatesAutoresizingMaskIntoConstraints = false
        chartView.heightAnchor.constraint(equalToConstant: 44).isActive = true
        chartView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor).isActive = true
        chartView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        chartView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8).isActive = true
        chartView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -16).isActive = true
        
        isEmptyLabel?.removeFromSuperview()
        isEmptyLabel = UILabel()
        isEmptyLabel.text = "Нет данных"
        isEmptyLabel.textColor = .blue
        isEmptyLabel.font = UIFont.boldSystemFont(ofSize: 18)
        isEmptyLabel.isHidden = true
        
        addSubview(isEmptyLabel)
        
        isEmptyLabel.translatesAutoresizingMaskIntoConstraints = false
        isEmptyLabel.centerXAnchor.constraint(equalTo: chartView.centerXAnchor).isActive = true
        isEmptyLabel.centerYAnchor.constraint(equalTo: chartView.centerYAnchor).isActive = true
        
        didChangeValue(segmentedControl)
    }
    
    private func setChartData(values: [[Date: Double]]) {
        isEmptyLabel.isHidden = !(values.first?.isEmpty ?? true)
        
        var sets = [LineChartDataSet]()
        
        
        var i = 0
        for valuesItem in values {
            var keys = [Date]()
            for key in valuesItem.keys {
                keys.append(key)
            }
            keys = keys.sorted(by: { $0 < $1 })
            
            var chartListDataEntry = [ChartDataEntry]()
            for key in keys {
                chartListDataEntry.append(ChartDataEntry(x: key.timeIntervalSince1970, y: valuesItem[key]!))
            }

            let set = LineChartDataSet(values: chartListDataEntry, label: "")
            switch i {
            case 0: set.setColor(.blue)
            case 1: set.setColor(colorWithHexString(hexString: "FB9100"))
            case 2: set.setColor(colorWithHexString(hexString: "B1D81F"))
            case 3: set.setColor(colorWithHexString(hexString: "A941EB"))
            default: set.setColor(.blue)
            }
            set.lineWidth = 4
            set.highlightColor = .clear
            set.drawCirclesEnabled = false
            set.drawValuesEnabled = false
            set.formLineWidth = 1
            set.formSize = 15
            
            sets.append(set)
            i += 1
        }
        chartView.data = LineChartData(dataSets: sets)

        chartView.highlightValue(nil)
        
        chartView.animate(xAxisDuration: 0.5)
    }
    
    // MARK: - Actions
    
    @objc private func didChangeValue(_ sender: SegmentedControl) {
        chartView.xAxis.valueFormatter = DateValueFormatter(type: segmentedControlData[segmentedControl.selectedIndex])
        print(chartData)
        setChartData(values: chartData[segmentedControl.selectedIndex])
    }
}

func colorWithHexString(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
    
    // Convert hex string to an integer
    let hexint = Int(intFromHexString(hexStr: hexString))
    let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
    let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
    let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
    let alpha = alpha!
    
    // Create color object, specifying alpha as well
    let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
    return color
}

func intFromHexString(hexStr: String) -> UInt32 {
    var hexInt: UInt32 = 0
    // Create scanner
    let scanner: Scanner = Scanner(string: hexStr)
    // Tell scanner to skip the # character
    scanner.charactersToBeSkipped = CharacterSet(charactersIn: "#")
    // Scan hex value
    scanner.scanHexInt32(&hexInt)
    return hexInt
}


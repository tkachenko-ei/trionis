//
//  StockExchangeValueCell.swift
//  Trionis
//
//  Created by Evgeniy on 04.08.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit

class StockExchangeValueCell: UITableViewCell {

    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var updateLabel: UILabel!
    
}

//
//  SegmentedControl.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit

@IBDesignable class SegmentedControl: UIControl {

    private var labels = [UILabel]()
    var thumbView = UIView()
    var isLeft = false

    var items: [String] = ["Item 1", "Item 2"] {
        didSet {
            setupLabels()
        }
    }

    var selectedIndex: Int = 0 {
        didSet {
            displayNewSelectedIndex()
        }
    }

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setupView()
    }

    // MARK: - Setup

    func setupView() {
        backgroundColor = UIColor.clear
        setupLabels()
        insertSubview(thumbView, at: 0)
    }

    func setupLabels() {
        for label in labels {
            label.removeFromSuperview()
        }

        labels.removeAll(keepingCapacity: true)

        for index in 1...items.count {
            let label = UILabel()
            label.text = items[index - 1]
            label.textAlignment = .center
            label.textColor = UIColor.lightGray
            label.font = UIFont.boldSystemFont(ofSize: 17)
            self.addSubview(label)
            labels.append(label)
        }

        thumbView.frame = CGRect(x: labels[selectedIndex].frame.origin.x + 10, y: self.bounds.height, width: labels[selectedIndex].frame.width - 20, height: 1)
        thumbView.backgroundColor = UIColor.blue

        self.addSubview(thumbView)
    }

    func changeFontInLabels(font: UIFont) {
        for label in labels {
            label.font = font
        }

        thumbView.frame = CGRect(x: labels[selectedIndex].frame.origin.x + 10, y: self.bounds.height, width: labels[selectedIndex].frame.width - 20, height: 1)
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        var labelsWidth = CGFloat(0)
        for index in 0...labels.count - 1 {
            let label = labels[index]
            labelsWidth += label.text!.widthWithConstrainedHeight(height: bounds.height, font: label.font)
        }

        var currentXPosition = (self.bounds.width - labelsWidth) / CGFloat(labels.count + 1)
        if isLeft {
            currentXPosition = 0
        }

        for index in 0...labels.count - 1 {
            let label = labels[index]
            
            if index == selectedIndex {
                label.textColor = .blue
            } else {
                label.textColor = .lightGray
            }

            let labelHeight = self.bounds.height - 1
            let labelWidth = label.text!.widthWithConstrainedHeight(height: labelHeight, font: label.font)

            label.frame = CGRect(x: currentXPosition, y: 0, width: labelWidth, height: labelHeight)

            if isLeft {
                currentXPosition = labelWidth + 40
            }else{
                currentXPosition += (self.bounds.width - labelsWidth) / CGFloat(labels.count + 1) + labelWidth
            }
        }

        thumbView.frame = CGRect(x: labels[selectedIndex].frame.origin.x, y: 0, width: labels[selectedIndex].frame.width, height: 2)
    }

    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        let location = touch.location(in: self)

        var calculatedIndex: Int?
        for (index, item) in labels.enumerated() {
            if item.frame.contains(location) {
                calculatedIndex = index
            }
        }

        if calculatedIndex != nil {
            selectedIndex = calculatedIndex!
            sendActions(for: .valueChanged)
        }

        return false
    }

    func displayNewSelectedIndex() {
        thumbView.frame = CGRect(x: labels[selectedIndex].frame.origin.x, y: 0, width: labels[selectedIndex].frame.width, height: 2)

    }

    func getFrameLabel(index: Int) -> CGRect? {
        if index < 0 && index > labels.count {
            return nil
        }

        return labels[index].frame
    }
}

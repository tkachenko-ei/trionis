//
//  GlobalFunctions.swift
//  Trionis
//
//  Created by Evgeniy on 09.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit

func showAlert(_ viewController: UIViewController, title: String?, message: String?, completion: (()-> Void)? = nil) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
    viewController.present(alert, animated: true, completion: completion)
}

func showSignIn(viewController: UIViewController) {
    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
    if let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController {
        navigationController.modalPresentationStyle = .overFullScreen
        viewController.present(navigationController, animated: true, completion: nil)
    }
}

//
//  DateValueFormatter.swift
//  Trionis
//
//  Created by Evgeniy on 01.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import Foundation
import Charts

public class DateValueFormatter: NSObject, IAxisValueFormatter {
    private let dateFormatter = DateFormatter()
    
    
    var currentTypeDateValueFormatter = ChartAndSegmentedControlCell.TypePeriod.today
    
    init(type: ChartAndSegmentedControlCell.TypePeriod) {
        super.init()
        
        dateFormatter.locale = Locale(identifier: "ru")
        
        currentTypeDateValueFormatter = type
        
        switch currentTypeDateValueFormatter {
        case .today:
            dateFormatter.dateFormat = "HH:mm"
        case .week:
            dateFormatter.dateFormat = "dd MMM"
        case .month:
            dateFormatter.dateFormat = "dd MMM"
        case .threeMounths:
            dateFormatter.dateFormat = "dd MMM"
        case .sixMounths:
            dateFormatter.dateFormat = "dd MMM"
        case .year:
            dateFormatter.dateFormat = "MMM"
        case .allPeriod:
            dateFormatter.dateFormat = "MMM YY"
        }
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return dateFormatter.string(from: Date(timeIntervalSince1970: value))
    }
}


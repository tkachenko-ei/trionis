//
//  UIViewController+Extension.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

extension UIViewController: NVActivityIndicatorViewable {
    
    func startLoading() {
        startAnimating(color: .white, minimumDisplayTime: 1, backgroundColor: UIColor.black.withAlphaComponent(0.3))
    }
    
    @objc func stopLoading() {
        stopAnimating()
    }
}

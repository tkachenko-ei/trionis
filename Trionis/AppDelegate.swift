//
//  AppDelegate.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    
    var deviceToken: String? = nil
    var fcmToken: String? = nil
    
    override init() {
        super.init()
        
        UIFont.overrideInitialize()
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        
        UNUserNotificationCenter.current().delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        application.registerForRemoteNotifications()
        
        print(Date(timeInterval: -(365 * 24 * 3600), since: Date()))
        
        UINavigationBar.appearance().titleTextAttributes = [
            kCTFontAttributeName: UIFont(name: "MyriadPro-Semibold", size: 20)!
            ] as [NSAttributedStringKey : Any]
        
        return true
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        if let refreshedToken = Messaging.messaging().fcmToken {
            self.fcmToken = refreshedToken
            self.deviceToken = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
            
            Messaging.messaging().subscribe(toTopic: "news") { error in
                print("Subscribed to news topic")
            }
            
            Messaging.messaging().subscribe(toTopic: "price") { error in
                print("Subscribed to price topic")
            }
            
            print("FCM token: \(String(describing: self.fcmToken))")
            print("Device token: \(String(describing: self.deviceToken))")
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {}
    
    func applicationWillEnterForeground(_ application: UIApplication) {}
    
    func applicationDidBecomeActive(_ application: UIApplication) {}
    
    func applicationWillTerminate(_ application: UIApplication) {}
    
}


//
//  APIManager.swift
//  Trionis
//
//  Created by Evgeniy on 09.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SCrypto

class APIManager {

    // MARK: - Shared Instance
    
    static let shared = APIManager()

    let URL = "http://info.trionis.su"
    
    // MARK: - Send Request
    
    private func sendRequest(url: String, method: HTTPMethod, parameters: Parameters?, encoding: ParameterEncoding, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: nil).responseJSON { (responseData) in
            switch responseData.result {
            case .success(let data):
                
                let value = Float(responseData.timeline.totalDuration)
                let roundedValue = roundf(value * 100) / 100
                debugPrint("SUCCESS : \(method.rawValue) - \(responseData.response!.statusCode) : \(responseData.request!.url!) : \(roundedValue) sec")
                debugPrint("Parameters:")
                debugPrint(parameters)
                debugPrint("Response data:")
                debugPrint(JSON(data))
                
                let json = JSON(data)
                if json["status"].stringValue == "ok" {
                    completion?(json, nil)
                } else {
                    completion?(nil, json["msg"].string ?? "error")
                }
                
            case .failure(let error):
                
                if let url = responseData.request?.url {
                    debugPrint("ERROR : \(responseData.response?.statusCode ?? 0) : \(url.absoluteString)")
                } else {
                    debugPrint("ERROR : \(responseData.response?.statusCode ?? 0)")
                }
                debugPrint("Parameters : \(parameters!)")
                debugPrint("Response data: \(String(data: responseData.data ?? Data(), encoding: String.Encoding.utf8)!)")
                
                completion?(nil, error.localizedDescription)
            }
        }
    }
    
    // Metods
    
    // MARK: - User
    
    func signIn(email: String, password: String, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/users/login"
        var parameters = ["email" : email,
                          "password" : password.MD5()]
        
        if let fcmToken = (UIApplication.shared.delegate as? AppDelegate)?.fcmToken {
            parameters["fcm_code"] = fcmToken
        } else {
            parameters["fcm_code"] = "jsdfjhksdgfhjkasgdfsgfjasgfkjhsgfj"
        }
        
        sendRequest(url: url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func signUp(fullname: String, email: String, organization: String, phone: String, stationId: [Int], completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/users/register"
        var parameters = ["os" : 2,
                          "fullname" : fullname,
                          "email" : email,
                          "organization" : organization,
                          "phone" : phone,
                          "stations" : stationId] as [String : Any]
        
        if let fcmToken = (UIApplication.shared.delegate as? AppDelegate)?.fcmToken {
            parameters["fcm_code"] = fcmToken
        } else {
            parameters["fcm_code"] = "jsdfjhksdgfhjkasgdfsgfjasgfkjhsgfj"
        }
        
        sendRequest(url: url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func validateUser(token: String, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/users/validate"
        let parameters = ["token" : token]
        
        sendRequest(url: url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, completion: completion)
    }
    
    func restorePassword(email: String, completion: ((_ error: String?)->Void)?) {
        let url = URL + "/api_v1/users/restore"
        let parameters = ["email" : email]
        
        sendRequest(url: url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted) { (json, error) in
            completion?(error)
        }
    }
    
    // MARK: - Other
    
    func getAllNews(page: Int, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/news"
        let parameters = ["page" : page]
        
        sendRequest(url: url, method: .get, parameters: parameters, encoding: URLEncoding.default) { (json, error) in
            completion?(json, error)
        }
    }
    
    func getNews(id: Int, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/news"
        let parameters = ["id" : id]
        
        sendRequest(url: url, method: .get, parameters: parameters, encoding: URLEncoding.default) { (json, error) in
            completion?(json, error)
        }
    }
    
    func getExchangeRates(page: Int, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/stat"
        let parameters = ["limit" : 10000]
        
        sendRequest(url: url, method: .get, parameters: parameters, encoding: URLEncoding.default) { (json, error) in
            completion?(json, error)
        }
    }
    
    func getStockExchange(completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/market"
        
        sendRequest(url: url, method: .get, parameters: nil, encoding: URLEncoding.default) { (json, error) in
            completion?(json?["data"], error)
        }
    }
    
    func getStations(token: String? = nil, page: Int, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/stations"
        var parameters = ["page": page] as [String : Any]
        if let token = token {
            parameters["token"] = token
        }
        sendRequest(url: url, method: .get, parameters: parameters, encoding: URLEncoding.default) { (json, error) in
            completion?(json, error)
        }
    }
    
    func getPrices(stationId: Int, day: Int, completion: ((_ json: JSON?, _ error: String?)->Void)?) {
        let url = URL + "/api_v1/prices/monthly"
        var parameters = ["station_id" : stationId,
                          "filter" : 6,
                          "day" : day] as [String : Any]
        
        if let token = UserManager.shared.getUserToken() {
            parameters["token"] = token
        } else {
            completion?(nil, "Необходима авторизация")
            return
        }
        
        sendRequest(url: url, method: .get, parameters: parameters, encoding: URLEncoding.default) { (json, error) in
            completion?(json, error)
        }
    }
    

}

//
//  UserManager.swift
//  Trionis
//
//  Created by Evgeniy on 17.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import Foundation

class UserManager {
    
    // MARK: - Shared Instance
    
    static let shared = UserManager()

    func saveUserToken(_ token: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(token, forKey: "userToken")
        userDefaults.synchronize()
    }
    
    func getUserToken() -> String? {
        let userDefaults = UserDefaults.standard
        return userDefaults.object(forKey: "userToken") as? String
    }
    
    func validateUser(success: (()->Void)?, error: (()->Void)?) {
        if let token = getUserToken() {
            APIManager.shared.validateUser(token: token) { (json, err) in
                if let _ = err {
                    error?()
                } else {
                    let userDefaults = UserDefaults.standard
                    userDefaults.set(json?["userrole"].stringValue, forKey: "userrole")
                    userDefaults.synchronize()
                    
                    success?()
                }
            }
        } else {
            error?()
        }
    }

}



//
//  ExchangeTVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ExchangeRates: String {
    case usd, euro, usdfix, eurofix, brent
}

class ExchangeTVC: UITableViewController {
    
    var currentExchangeRates: ExchangeRates = .usd {
        didSet {
            tableView.reloadData()
        }
    }
    
    var exchangeRates: [JSON] = [JSON]() {
        didSet {
            usdExchangeRates.removeAll()
            euroExchangeRates.removeAll()
            usdfixExchangeRates.removeAll()
            eurofixExchangeRates.removeAll()
            brentExchangeRates.removeAll()
            
            for exchangeRate in exchangeRates {
                usdExchangeRates[Date(timeIntervalSince1970: TimeInterval(exchangeRate["date"].intValue))] =
                Double(round(1000*exchangeRate["usd"].doubleValue)/1000)
                euroExchangeRates[Date(timeIntervalSince1970: TimeInterval(exchangeRate["date"].intValue))] = Double(round(1000*exchangeRate["euro"].doubleValue)/1000)
                usdfixExchangeRates[Date(timeIntervalSince1970: TimeInterval(exchangeRate["date"].intValue))] = Double(round(1000*exchangeRate["usdfix"].doubleValue)/1000)
                eurofixExchangeRates[Date(timeIntervalSince1970: TimeInterval(exchangeRate["date"].intValue))] = Double(round(1000*exchangeRate["eurofix"].doubleValue)/1000)
                brentExchangeRates[Date(timeIntervalSince1970: TimeInterval(exchangeRate["date"].intValue))] = Double(round(1000*exchangeRate["brent"].doubleValue)/1000)
            }
            
            tableView.reloadData()
        }
    }
    
    var usdExchangeRates: [Date: Double] = [:]
    var euroExchangeRates: [Date: Double] = [:]
    var usdfixExchangeRates: [Date: Double] = [:]
    var eurofixExchangeRates: [Date: Double] = [:]
    var brentExchangeRates: [Date: Double] = [:]
    
    // MARK: - Init

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CurrentExchangeRatesCell")
        tableView.register(ChartAndSegmentedControlCell.self, forCellReuseIdentifier: "ChartAndSegmentedControlCell")
        UserManager.shared.validateUser(success: nil, error: nil)
        loadExchangeRates()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    // MARK: - Load Exchange rates
    
    
    func loadExchangeRates() {
        startLoading()
        exchangeRates.removeAll()
        loadExchangeRatesOnePage(page: 0) {
            
            
            self.stopLoading()
            
            
        }
        
//        let myGroup = DispatchGroup()
//
//        for page in 0..<10 {
//            myGroup.enter()
//            loadExchangeRatesOnePage(page: page) {
//                myGroup.leave()
//            }
//        }
//
//        myGroup.notify(queue: .main) {
//            self.stopLoading()
//        }
    }
    
    func loadExchangeRatesOnePage(page: Int, completion: (()->Void)?) {
        APIManager.shared.getExchangeRates(page: page) { (json, error) in
            if let _ = error {
                completion?()
                return
            }
            
            if let json = json {
                self.exchangeRates += json["data"].arrayValue
                
                completion?()
                return
            }
            
            completion?()
        }
    }
    
    // MARK: - Setup Cells
    
    func setupCurrentExchangeRatesCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentExchangeRatesCell", for: indexPath)
        
        cell.selectionStyle = .default
        cell.accessoryType = .disclosureIndicator
        
        switch currentExchangeRates {
        case .usd:
            cell.textLabel?.text = "ЦБ РФ Доллар США"
        case .euro:
            cell.textLabel?.text = "ЦБ РФ Евро"
        case .usdfix:
            cell.textLabel?.text = "MOEX USDFIX"
        case .eurofix:
            cell.textLabel?.text = "MOEX EURFIX"
        case .brent:
            cell.textLabel?.text = "Нефть Brent"
        }
        
        return cell
    }
    
    func setupChartAndSegmentedControlCell(_ indexPath: IndexPath) -> ChartAndSegmentedControlCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartAndSegmentedControlCell", for: indexPath) as! ChartAndSegmentedControlCell
        
        cell.selectionStyle = .none
        
        switch currentExchangeRates {
        case .usd:
            var todayExchangeRates: [Date: Double] = [:]
            var weekExchangeRates: [Date: Double] = [:]
            var monthExchangeRates: [Date: Double] = [:]
            var yearExchangeRates: [Date: Double] = [:]
            for (key, value) in usdExchangeRates {
                if key.isDateInBetweenToday() {
                    todayExchangeRates[key] = value
                }
                if key.isDateInBetweenWeek() {
                    weekExchangeRates[key] = value
                }
                if key.isDateInBetweenMonth() {
                    monthExchangeRates[key] = value
                }
                if key.isDateInBetweenYear() {
                    yearExchangeRates[key] = value
                }
            }
            
            if todayExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if weekExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if monthExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if yearExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            cell.setupSegmentedControlAndChart(segmentedControlData: [.today, .week, .month, .year],
                                               chartData: [[todayExchangeRates], [weekExchangeRates], [monthExchangeRates], [yearExchangeRates]])
        case .euro:
            var todayExchangeRates: [Date: Double] = [:]
            var weekExchangeRates: [Date: Double] = [:]
            var monthExchangeRates: [Date: Double] = [:]
            var yearExchangeRates: [Date: Double] = [:]
            for (key, value) in euroExchangeRates {
                if key.isDateInBetweenToday() {
                    todayExchangeRates[key] = value
                }
                if key.isDateInBetweenWeek() {
                    weekExchangeRates[key] = value
                }
                if key.isDateInBetweenMonth() {
                    monthExchangeRates[key] = value
                }
                if key.isDateInBetweenYear() {
                    yearExchangeRates[key] = value
                }
            }
            
            if todayExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if weekExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if monthExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if yearExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            cell.setupSegmentedControlAndChart(segmentedControlData: [.today, .week, .month, .year],
                                               chartData: [[todayExchangeRates], [weekExchangeRates], [monthExchangeRates], [yearExchangeRates]])
        case .usdfix:
            var todayExchangeRates: [Date: Double] = [:]
            var weekExchangeRates: [Date: Double] = [:]
            var monthExchangeRates: [Date: Double] = [:]
            var yearExchangeRates: [Date: Double] = [:]
            for (key, value) in usdfixExchangeRates {
                if key.isDateInBetweenToday() {
                    todayExchangeRates[key] = value
                }
                if key.isDateInBetweenWeek() {
                    weekExchangeRates[key] = value
                }
                if key.isDateInBetweenMonth() {
                    monthExchangeRates[key] = value
                }
                if key.isDateInBetweenYear() {
                    yearExchangeRates[key] = value
                }
            }
            
            if todayExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if weekExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if monthExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if yearExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            cell.setupSegmentedControlAndChart(segmentedControlData: [.today, .week, .month, .year],
                                               chartData: [[todayExchangeRates], [weekExchangeRates], [monthExchangeRates], [yearExchangeRates]])
        case .eurofix:
            var todayExchangeRates: [Date: Double] = [:]
            var weekExchangeRates: [Date: Double] = [:]
            var monthExchangeRates: [Date: Double] = [:]
            var yearExchangeRates: [Date: Double] = [:]
            for (key, value) in eurofixExchangeRates {
                if key.isDateInBetweenToday() {
                    todayExchangeRates[key] = value
                }
                if key.isDateInBetweenWeek() {
                    weekExchangeRates[key] = value
                }
                if key.isDateInBetweenMonth() {
                    monthExchangeRates[key] = value
                }
                if key.isDateInBetweenYear() {
                    yearExchangeRates[key] = value
                }
            }
            
            if todayExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if weekExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if monthExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if yearExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            cell.setupSegmentedControlAndChart(segmentedControlData: [.today, .week, .month, .year],
                                               chartData: [[todayExchangeRates], [weekExchangeRates], [monthExchangeRates], [yearExchangeRates]])
        case .brent:
            var todayExchangeRates: [Date: Double] = [:]
            var weekExchangeRates: [Date: Double] = [:]
            var monthExchangeRates: [Date: Double] = [:]
            var yearExchangeRates: [Date: Double] = [:]
            for (key, value) in brentExchangeRates {
                if key.isDateInBetweenToday() {
                    todayExchangeRates[key] = value
                }
                if key.isDateInBetweenWeek() {
                    weekExchangeRates[key] = value
                }
                if key.isDateInBetweenMonth() {
                    monthExchangeRates[key] = value
                }
                if key.isDateInBetweenYear() {
                    yearExchangeRates[key] = value
                }
            }
            
            if todayExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if weekExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if monthExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            if yearExchangeRates.count == 1 {
                todayExchangeRates[Date()] = todayExchangeRates.first?.value
            }
            
            cell.setupSegmentedControlAndChart(segmentedControlData: [.today, .week, .month, .year],
                                               chartData: [[todayExchangeRates], [weekExchangeRates], [monthExchangeRates], [yearExchangeRates]])
        }

        return cell
    }
    
}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension ExchangeTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 44
        case 1:
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (navigationController?.navigationBar.frame.height ?? 0.0)
            let bottomBarHeight = (tabBarController?.tabBar.frame.height ?? 0.0)
            let screenHeight = UIScreen.main.bounds.height
            return screenHeight - topBarHeight - bottomBarHeight - 44 - 8
        default:
            return 0
        }

    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            return setupCurrentExchangeRatesCell(indexPath)
        case 1:
            return setupChartAndSegmentedControlCell(indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            let alert = UIAlertController(title: "Выберите курс", message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "ЦБ РФ Доллар США", style: .default, handler: { action in
                self.currentExchangeRates = .usd
                tableView.cellForRow(at: indexPath)?.textLabel?.text = "ЦБ РФ Доллар США"
            }))
            alert.addAction(UIAlertAction(title: "ЦБ РФ Евро", style: .default, handler: { action in
                self.currentExchangeRates = .euro
                tableView.cellForRow(at: indexPath)?.textLabel?.text = "ЦБ РФ Евро"
            }))
            alert.addAction(UIAlertAction(title: "MOEX USDFIX", style: .default, handler: { action in
                self.currentExchangeRates = .usdfix
                tableView.cellForRow(at: indexPath)?.textLabel?.text = "MOEX USDFIX"
            }))
            alert.addAction(UIAlertAction(title: "MOEX EURFIX", style: .default, handler: { action in
                self.currentExchangeRates = .usdfix
                tableView.cellForRow(at: indexPath)?.textLabel?.text = "MOEX EURFIX"
            }))
            alert.addAction(UIAlertAction(title: "Нефть Brent", style: .default, handler: { action in
                self.currentExchangeRates = .brent
                tableView.cellForRow(at: indexPath)?.textLabel?.text = "Нефть Brent"
            }))
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
}

public extension Date {
    
    func isDateInBetweenToday() -> Bool {
        let calendar = Calendar.current
        return calendar.isDateInToday(self)
    }
    
    func isDateInBetweenWeek() -> Bool {
        return self < Date() && self > Date(timeInterval: -(7 * 24 * 3600), since: Date())
    }
    
    func isDateInBetweenMonth() -> Bool {
        return self < Date() && self > Date(timeInterval: -(30 * 24 * 3600), since: Date())
    }
    
    func isDateInBetweenYear() -> Bool {
        return self < Date() && self > Date(timeInterval: -(365 * 24 * 3600), since: Date())
    }
    
    public static func randomWithinDaysBeforeToday(days: Int) -> Date {
        let today = Date()
        
        let gregorian = Calendar(identifier: .gregorian)
        
        let r1 = arc4random_uniform(UInt32(days))
        let r2 = arc4random_uniform(UInt32(23))
        let r3 = arc4random_uniform(UInt32(23))
        let r4 = arc4random_uniform(UInt32(23))
        
        var offsetComponents = DateComponents()
        offsetComponents.day = Int(r1) * -1
        offsetComponents.hour = Int(r2)
        offsetComponents.minute = Int(r3)
        offsetComponents.second = Int(r4)
        
        
        guard let rndDate1 = gregorian.date(byAdding: offsetComponents, to: today)else {
            print("randoming failed")
            return today
        }
        return rndDate1
    }
    
    /// SwiftRandom extension
    public static func random() -> Date {
        let randomTime = TimeInterval(arc4random_uniform(UInt32.max))
        return Date(timeIntervalSince1970: randomTime)
    }
    
}

//
//  StatisticsTVC.swift
//  Trionis
//
//  Created by Evgeniy on 01.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON

class StatisticsTVC: UITableViewController {

    let stationsPicker = UIPickerView()
    
    var stations: [JSON] = [JSON]()
    var currentStation: JSON?
    
    var currentMonth: [[[Date: Double]]] = [[[:]]]
    var currentMonthPlus1: [[[Date: Double]]] = [[[:]]]
    var currentMonthPlus2: [[[Date: Double]]] = [[[:]]]
    var currentMonthPlus3: [[[Date: Double]]] = [[[:]]]
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CurrentStationCell")
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "CurrentMonthCell")
        tableView.register(ChartAndSegmentedControlCell.self, forCellReuseIdentifier: "ChartAndSegmentedControlCell")
        
        startLoading()
        UserManager.shared.validateUser(success: {
            self.getStations()
        }) {
            self.stopLoading()
            showSignIn(viewController: self)
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    func getStations() {
        startLoading()
        APIManager.shared.getStations(page: 1) { (json, error) in
            if let error = error {
                self.stopLoading()
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stations = json!["data"].arrayValue
            self.currentStation = self.stations.first
            self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.textLabel?.text = self.stations.first?["name"].stringValue ?? ""
            
            APIManager.shared.getStations(page: 2) { (json, error) in
                if let error = error {
                    self.stopLoading()
                    showAlert(self, title: error, message: nil)
                    return
                }
                
                self.stations += json!["data"].arrayValue
                self.loadExchangeRates()
            }
        }
    }
    
    // MARK: - Load Exchange rates
    
    func loadExchangeRates() {
        startLoading()
        let values: Double = randomNumber()
        let date1 = Date(timeIntervalSince1970: 1532736000)
        let date2 = Date(timeIntervalSince1970: 1532908800)
        let date3 = Date(timeIntervalSince1970: 1533168000)
        let date4 = Date(timeIntervalSince1970: 1533859200)
        currentMonth = [[[date1: values, date2: values - 20, date3: values, date4: values]],
                        [[date1: values, date2: values - 20, date3: values, date4: values]],
                        [[date1: values, date2: values - 20, date3: values, date4: values]],
                        [[date1: values, date2: values - 20, date3: values, date4: values]]]
        currentMonthPlus1 = [[[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4]]]
        currentMonthPlus2 = [[[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 5, date3: values - 8, date4: values - 7]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 6, date3: values - 7, date4: values - 8]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 6, date3: values - 8, date4: values - 8]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 7, date3: values - 8, date4: values - 8]]]
        currentMonthPlus3 = [[[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 5, date3: values - 8, date4: values - 7],
                              [date1: values - 10, date2: values - 20 - 8, date3: values - 12, date4: values - 13]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 6, date3: values - 7, date4: values - 8],
                              [date1: values - 10, date2: values - 20 - 9, date3: values - 16, date4: values - 14]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 6, date3: values - 8, date4: values - 8],
                              [date1: values - 10, date2: values - 20 - 9, date3: values - 16, date4: values - 14]],
                             [[date1: values, date2: values - 20, date3: values, date4: values],
                              [date1: values - 4, date2: values - 20 - 3, date3: values - 5, date4: values - 4],
                              [date1: values - 7, date2: values - 20 - 7, date3: values - 8, date4: values - 8],
                              [date1: values - 10, date2: values - 20 - 9, date3: values - 15, date4: values - 14]]]
        self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
        
        perform(#selector(stopLoading), with: self, afterDelay: 1)
    }
    
    // MARK: - Setup Cells
    
    func currentStationCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentStationCell", for: indexPath)
        
        cell.selectionStyle = .default
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    func currentMonthCell(_ indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CurrentStationCell", for: indexPath)
        
        cell.selectionStyle = .default
        cell.accessoryType = .disclosureIndicator
        
        cell.textLabel?.text = "Текущий"
        
        return cell
    }
    
    func setupChartAndSegmentedControlCell(_ indexPath: IndexPath) -> ChartAndSegmentedControlCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartAndSegmentedControlCell", for: indexPath) as! ChartAndSegmentedControlCell
        
        cell.selectionStyle = .none
        
        switch self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text {
        case "Текущий":
            cell.setupSegmentedControlAndChart(segmentedControlData: [.month, .threeMounths, .sixMounths, .allPeriod], chartData: currentMonth)
        case "Текущий, +1":
            cell.setupSegmentedControlAndChart(segmentedControlData: [.month, .threeMounths, .sixMounths, .allPeriod], chartData: currentMonthPlus1)
        case "Текущий, +1, +2":
            cell.setupSegmentedControlAndChart(segmentedControlData: [.month, .threeMounths, .sixMounths, .allPeriod], chartData: currentMonthPlus2)
        case "Текущий, +1, +2, +3":
            cell.setupSegmentedControlAndChart(segmentedControlData: [.month, .threeMounths, .sixMounths, .allPeriod], chartData: currentMonthPlus3)
        default:
            cell.setupSegmentedControlAndChart(segmentedControlData: [.month, .threeMounths, .sixMounths, .allPeriod], chartData: currentMonth)
        }

        return cell
    }

}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension StatisticsTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 2
        case 1:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 44
        case 1:
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height + (navigationController?.navigationBar.frame.height ?? 0.0)
            let bottomBarHeight = (tabBarController?.tabBar.frame.height ?? 0.0)
            let screenHeight = UIScreen.main.bounds.height
            return screenHeight - topBarHeight - bottomBarHeight - 2 * 44 - 8
        default:
            return 0
        }
        
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                return currentStationCell(indexPath)
            case 1:
                return currentMonthCell(indexPath)
            default:
                return UITableViewCell()
            }
        case 1:
            return setupChartAndSegmentedControlCell(indexPath)
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 && indexPath.row == 0 {
            let alert = UIAlertController(title: "Выберите станцию", message: nil, preferredStyle: .actionSheet)
            
            for station in stations {
                alert.addAction(UIAlertAction(title: station["name"].stringValue, style: .default, handler: { action in
                    self.currentStation = station
                    self.tableView.cellForRow(at: IndexPath(row: 0, section: 0))?.textLabel?.text = station["name"].stringValue
                    self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text = "Текущий"
                    self.loadExchangeRates()
                }))
            }
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
        
        if indexPath.section == 0 && indexPath.row == 1 {
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "Текущий", style: .default, handler: { action in
                self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text = "Текущий"
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }))
            
            alert.addAction(UIAlertAction(title: "Текущий, +1", style: .default, handler: { action in
                self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text = "Текущий, +1"
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }))
            
            alert.addAction(UIAlertAction(title: "Текущий, +1, +2", style: .default, handler: { action in
                self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text = "Текущий, +1, +2"
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }))
            
            alert.addAction(UIAlertAction(title: "Текущий, +1, +2, +3", style: .default, handler: { action in
                self.tableView.cellForRow(at: IndexPath(row: 1, section: 0))?.textLabel?.text = "Текущий, +1, +2, +3"
                self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
            }))
            
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
}

func randomNumber(range: ClosedRange<Int> = 660...690) -> Double {
    let min = Double(range.lowerBound)
    let max = Double(range.upperBound)
    return Double(arc4random_uniform(UInt32(1 + max - min))) + min
}


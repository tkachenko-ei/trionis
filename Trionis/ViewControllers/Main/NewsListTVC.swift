//
//  NewsListTVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class NewsListTVC: UITableViewController {
    
    var loadingNewsList = false
    var currentPageNewsList = 1
    var newsList = [JSON]()
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "NewsCell", bundle: nil), forCellReuseIdentifier: "NewsCell")
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshingNews), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        loadNews()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    // MARK: - Loading news
   
    func loadNews() {
        if currentPageNewsList == 1 {
            startLoading()
        }
        loadingNewsList = true
        APIManager.shared.getAllNews(page: currentPageNewsList) { (json, error) in
            self.loadingNewsList = true
            self.stopLoading()
            self.tableView.refreshControl?.endRefreshing()
            
            if let error = error {
                showAlert(self, title: error, message: nil)
                return
            }

            self.currentPageNewsList += 1
            self.newsList = json!["data"].arrayValue
            self.tableView.reloadData()
        }
    }
    
    @objc func refreshingNews() {
        currentPageNewsList = 1
        loadNews()
    }
    
    // MARK: - Segua
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showNewsDetails" {
            let newViewController: NewsDetailsTVC = segue.destination as! NewsDetailsTVC
            if let index = tableView.indexPathForSelectedRow?.row {
                newViewController.news = newsList[index]
            }
        }
    }

}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension NewsListTVC {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as! NewsCell
        
        if let image = newsList[indexPath.row]["images"].arrayValue.first?["image_url"].stringValue {
            let placeholder = UIImage.imageWithColor(color: .lightGray)
            let url = URL(string: APIManager.shared.URL + image)
            cell.posterImage.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
        } else {
            cell.posterImage.image = UIImage.imageWithColor(color: .lightGray)
        }
        
        cell.titleLabel.text = newsList[indexPath.row]["title"].stringValue
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.locale = Locale(identifier: "ru")
        let date = Date(timeIntervalSince1970: TimeInterval(Int(newsList[indexPath.row]["date"].stringValue) ?? 0))
        cell.dateLabel.text = dateFormatter.string(from: date)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showNewsDetails", sender: self)
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        let deltaOffset = maximumOffset - currentOffset
        
        if deltaOffset <= 300 && !loadingNewsList {
            loadNews()
        }
    }
}


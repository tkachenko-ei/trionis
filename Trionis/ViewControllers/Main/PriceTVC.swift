//
//  PriceTVC.swift
//  Trionis
//
//  Created by Evgeniy on 01.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON
import SwiftDate

class PriceTVC: UITableViewController {
    
    @IBOutlet weak var searchTextFieild: UITextField!
    
    @IBOutlet weak var updateDateLabel: UILabel!
    
    @IBOutlet weak var month1Label: UILabel!
    @IBOutlet weak var month2Label: UILabel!
    @IBOutlet weak var month3Label: UILabel!
    @IBOutlet weak var month4Label: UILabel!
    @IBOutlet weak var price1Label: UILabel!
    @IBOutlet weak var price2Label: UILabel!
    @IBOutlet weak var price3Label: UILabel!
    @IBOutlet weak var price4Label: UILabel!
    @IBOutlet weak var difference1Label: UILabel!
    @IBOutlet weak var difference2Label: UILabel!
    @IBOutlet weak var difference3Label: UILabel!
    @IBOutlet weak var difference4Label: UILabel!
    
    
    let stationsPicker = UIPickerView()
    
    var stations: [JSON] = [JSON]()
    var currentStation: JSON?
    
    var priceData: JSON? {
        didSet {
            var date = Date()
            let dateFormatter = DateFormatter()
            dateFormatter.locale = Locale(identifier: "ru_UA")
            dateFormatter.dateFormat = "dd MMM"
            
            month1Label.text = "\(dateFormatter.string(from: date).uppercased())"
            
            date = Date() + 1.months

            month2Label.text = "\(dateFormatter.string(from: date).uppercased())"
            
            date = Date() + 2.months
            month3Label.text = "\(dateFormatter.string(from: date).uppercased())"
            
            date = Date() + 3.months
            month4Label.text = "\(dateFormatter.string(from: date).uppercased())"
            
            price1Label.text = (priceData?["price"].string ?? "") + "$ / т"
            price2Label.text = (priceData?["price_plus_1"].string ?? "") + "$ / т"
            price3Label.text = (priceData?["price_plus_2"].string ?? "") + "$ / т"
            price4Label.text = (priceData?["price_plus_3"].string ?? "") + "$ / т"
            
            difference1Label.text = "\((priceData?["price"].int ?? 0) - (priceData?["price"].int ?? 0))"
            difference1Label.backgroundColor = (priceData?["price"].int ?? 0) - (priceData?["price"].int ?? 0) > 0 ? .green : .red
            if difference1Label.text == "0" {
                difference1Label.backgroundColor = .lightGray
            }
            difference2Label.text = "\((priceData?["price_plus_1"].intValue ?? 0) - (priceData?["price"].intValue ?? 0))"
            difference2Label.backgroundColor = (priceData?["price_plus_1"].intValue ?? 0) - (priceData?["price"].intValue ?? 0) > 0 ? .green : .red
            if difference2Label.text == "0" {
                difference2Label.backgroundColor = .lightGray
            }
            difference3Label.text = "\((priceData?["price_plus_2"].intValue ?? 0) - (priceData?["price_plus_1"].intValue ?? 0))"
            difference3Label.backgroundColor = (priceData?["price_plus_2"].intValue ?? 0) - (priceData?["price_plus_1"].intValue ?? 0) > 0 ? .green : .red
            if difference3Label.text == "0" {
                difference3Label.backgroundColor = .lightGray
            }
            difference4Label.text = "\((priceData?["price_plus_3"].intValue ?? 0) - (priceData?["price_plus_2"].intValue ?? 0))"
            difference4Label.backgroundColor = (priceData?["price_plus_3"].intValue ?? 0) - (priceData?["price_plus_2"].intValue ?? 0) > 0 ? .green : .red
            if difference4Label.text == "0" {
                difference4Label.backgroundColor = .lightGray
            }

            var data = [Date: Double]()
            
            if let price = priceData?["price"].doubleValue {
                data[Date()] = price
            }
            if let price = priceData?["price_plus_1"].doubleValue {
                data[Date() + 1.months] = price
            }
            if let price = priceData?["price_plus_2"].doubleValue {
                data[Date() + 2.months] = price
            }
            if let price = priceData?["price_plus_3"].doubleValue {
                data[Date() + 3.months] = price
            }
            
            if let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 2)) as? ChartCell {
                cell.setupChart(chartData: data)
            }
        }
    }
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextFieild.inputView = stationsPicker
        stationsPicker.delegate = self
        stationsPicker.dataSource = self
        setupStationsDatePicker()
        
//        tableView.register(PricesCell.self, forCellReuseIdentifier: "PricesCell")
//        tableView.register(CharCell.self, forCellReuseIdentifier: "CharCell")
        
        startLoading()
        UserManager.shared.validateUser(success: {
            self.getStations()
        }) {
            self.stopLoading()
            showSignIn(viewController: self)
        }

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    func congigUpdateDate() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.locale = Locale(identifier: "ru_UA")
        
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        timeFormatter.locale = Locale(identifier: "ru_UA")
        
        updateDateLabel.text = "Обновленно: \(dateFormatter.string(from: Date())) в \(timeFormatter.string(from: Date()))"
    }
    
    func getStations() {
        startLoading()
        APIManager.shared.getStations(token: UserManager.shared.getUserToken(), page: 1) { (json, error) in
            if let error = error {
                self.stopLoading()
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stations = json!["data"].arrayValue
            self.currentStation = self.stations.first
            self.searchTextFieild.text = self.stations.first?["name"].stringValue ?? ""
            
            self.loadPrices()
        }
    }
    
    // MARK: - Setup Date Picker
    
    private func setupStationsDatePicker() {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.tintColor = .blue
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(dismissKeyboard))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        searchTextFieild.inputAccessoryView = toolBar
        searchTextFieild.inputView = stationsPicker
    }
    
    // MARK: - Keyboard
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        self.loadPrices()
    }
    
    // MARK: - Load Prices
    
    func loadPrices() {
        congigUpdateDate()
        startLoading()
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        APIManager.shared.getPrices(stationId: currentStation?["station_id"].intValue ?? 0, day: day) { (json, error) in
            self.stopLoading()
            
            if let error = error {
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.priceData = json?["data"].arrayValue.first
        }
    }
    
    func setupChartAndSegmentedControlCell(_ indexPath: IndexPath) -> ChartAndSegmentedControlCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChartAndSegmentedControlCell", for: indexPath) as! ChartAndSegmentedControlCell
        
        return cell
    }
    
    @IBAction func didPressShare(_ sender: UIButton) {
        if #available(iOS 11.0, *) {
            let image = (UIApplication.shared.delegate as! AppDelegate).window!.toImage()
            
            let imageToShare = [image]
            let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
            
            // exclude some activity types from the list (optional)
            activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
            
            // present the view controller
            self.present(activityViewController, animated: true, completion: nil)
        } else {
            showAlert(self, title: "Ошибка", message: "Версия Вашей операционной системы не поддерживает работу скриншотов, обновите, пожалуйста, операционную систему.")
        }
        
    }
    

}

extension PriceTVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stations.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return stations[row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        searchTextFieild.text = stations[row]["name"].stringValue
        currentStation = stations[row]
    }
}

extension UIView {
    func toImage() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}

//
//  StockExchangeTVC.swift
//  Trionis
//
//  Created by Evgeniy on 04.08.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON

class StockExchangeTVC: UITableViewController {
    
    var quotes: [JSON] = [JSON]()
    var settlement: [JSON] = [JSON]()

    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "StockExchangeTypeCell")
        tableView.register(UINib(nibName: "StockExchangeHeaderCell", bundle: nil), forCellReuseIdentifier: "StockExchangeHeaderCell")
        tableView.register(UINib(nibName: "StockExchangeValueCell", bundle: nil), forCellReuseIdentifier: "StockExchangeValueCell")
        tableView.tableFooterView = UIView()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(loadData), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        startLoading()
        UserManager.shared.validateUser(success: {
            self.loadData()
        }) {
            self.stopLoading()
            showSignIn(viewController: self)
        }
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    @objc func loadData() {
        startLoading()
        APIManager.shared.getStockExchange { (json, error) in
            if let error = error {
                self.stopLoading()
                self.tableView.refreshControl?.endRefreshing()
                showAlert(self, title: error, message: nil)
                return
            }
            
            if let json = json {
                self.quotes = json["quotes"].arrayValue
                self.settlement = json["settlement"].arrayValue
                self.stopLoading()
                self.tableView.refreshControl?.endRefreshing()
                self.tableView.reloadData()
                return
            }
            
            self.loadData()
        }
    }

}

// MARK: - UITableViewDelegate and UITableViewDataSource

extension StockExchangeTVC {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return !quotes.isEmpty ? 2 + quotes.count : 0
        case 1:
            return !settlement.isEmpty ? 2 + settlement.count : 0
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeTypeCell", for: indexPath)
                cell.textLabel?.text = "Quotes:"
                cell.textLabel?.font = UIFont(name: "MyriadPro-Semibold", size: 17)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeHeaderCell", for: indexPath) as! StockExchangeHeaderCell
                cell.updateLabel.isHidden = false
                return cell
            case 2...100:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeValueCell", for: indexPath) as! StockExchangeValueCell
                
                let data = quotes[indexPath.row - 2]
                cell.monthLabel.text = data["Month"].stringValue
                cell.valueLabel.text = data["Change"].stringValue
                if Double(data["Change"].stringValue) ?? 0 >= 0 {
                    cell.valueLabel.textColor = .green
                } else {
                    cell.valueLabel.textColor = .red
                }
                cell.updateLabel.text = data["Updated"].stringValue
                cell.updateLabel.isHidden = false
                
                return cell
            default:
                return UITableViewCell()
            }
        case 1:
            switch indexPath.row {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeTypeCell", for: indexPath)
                cell.textLabel?.text = "Settlement:"
                cell.textLabel?.font = UIFont(name: "MyriadPro-Semibold", size: 17)
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeHeaderCell", for: indexPath) as! StockExchangeHeaderCell
                cell.updateLabel.isHidden = true
                return cell
            case 2...100:
                let cell = tableView.dequeueReusableCell(withIdentifier: "StockExchangeValueCell", for: indexPath) as! StockExchangeValueCell
                
                let data = settlement[indexPath.row - 2]
                cell.monthLabel.text = data["Month"].stringValue
                cell.valueLabel.text = data["Change"].stringValue
                if Double(data["Change"].stringValue) ?? 0 >= 0 {
                    cell.valueLabel.textColor = .green
                } else {
                    cell.valueLabel.textColor = .red
                }
                cell.updateLabel.isHidden = true
                
                return cell
            default:
                return UITableViewCell()
            }
        default:
            return UITableViewCell()
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section != 0 ? 8 : 0
    }
}

//
//  TabBarController.swift
//  Trionis
//
//  Created by Evgeniy on 23.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        
        self.selectedIndex = 4
    }
    
    func showPopup() {
        let alert = UIAlertController(title: "Для использования этого функционала нужна авторизация", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Авторизация", style: .destructive, handler: { action in
            showSignIn(viewController: self)
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

}

extension TabBarController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        
        switch viewController {
        case viewControllers![3]:
            let userDefaults = UserDefaults.standard
            if let userrole = userDefaults.object(forKey: "userrole") as? String {
                if userrole == "3" {
                    let alert = UIAlertController(title: "Вкладка недоступна", message: nil, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Продолжить", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    return false
                }
            }
        default:
            break
        }
        
        return true
    }
    
}

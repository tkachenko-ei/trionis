//
//  NewsDetailsTVC.swift
//  Trionis
//
//  Created by Evgeniy on 18.07.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class NewsDetailsTVC: UITableViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var news: JSON?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "MyriadPro-Regular", size: 20)!]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let news = news {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.locale = Locale(identifier: "ru")
            let date = Date(timeIntervalSince1970: TimeInterval(Int(news["date"].stringValue) ?? 0))
            dateLabel.text = dateFormatter.string(from: date)
            
            if let imagesJSON = news["images"].array {
                for i in 0..<imagesJSON.count {
                    let frame = CGRect(x: CGFloat(i) * UIScreen.main.bounds.width, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width * 9 / 16)
                    let imageView = UIImageView(frame: frame)
                    imageView.contentMode = .scaleAspectFill
                    imageView.clipsToBounds = true
                    let placeholder = UIImage.imageWithColor(color: .lightGray)
                    let url = URL(string: APIManager.shared.URL + imagesJSON[i]["image_url"].stringValue)
                    imageView.kf.setImage(with: url, placeholder: placeholder, options: nil, progressBlock: nil, completionHandler: nil)
                    scrollView.addSubview(imageView)
                    scrollView.contentSize.width += UIScreen.main.bounds.width
                }
            }
            
            titleLabel.text = news["title"].string
            descriptionTextView.text = news["text"].string
        }
        descriptionTextView.contentInset.left = 16
        descriptionTextView.contentInset.right = 16
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.height - tabBarController!.tabBar.frame.size.height - 16
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}

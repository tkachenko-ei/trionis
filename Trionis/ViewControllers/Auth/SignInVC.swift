//
//  SignInVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftValidators

class SignInVC: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForKeyboardNotifications()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))

        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 2
        emailTextField.setLeftPaddingPoints(16)
        emailTextField.setRightPaddingPoints(16)
        codeTextField.layer.borderColor = UIColor.lightGray.cgColor
        codeTextField.layer.borderWidth = 1
        codeTextField.layer.cornerRadius = 2
        codeTextField.setLeftPaddingPoints(16)
        codeTextField.setRightPaddingPoints(16)
        
        signUpButton.layer.borderColor = UIColor.lightGray.cgColor
        signUpButton.layer.borderWidth = 1
        signUpButton.layer.cornerRadius = 2
        
        signInButton.layer.cornerRadius = 2
        
    }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    // MARK: - Keyboard
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    private func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView.contentInset.bottom = keyboardSize.height - (UIScreen.main.bounds.height - signUpButton.frame.origin.y - signUpButton.frame.height) + 36
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchSignIn(_ sender: UIButton) {
        dismissKeyboard()
        
        if !Validator.isEmail().apply(emailTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите email", message: nil)
            return
        }
        
        if Validator.isEmpty().apply(codeTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите код доступа", message: nil)
            return
        }
        
        startLoading()
        APIManager.shared.signIn(email: emailTextField.text!.trimmingTrailingSpaces(), password: codeTextField.text!.trimmingTrailingSpaces()) { (json, error) in
            if let error = error {
                self.stopLoading()
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stopLoading()
            
            UserManager.shared.saveUserToken(json!["token"].stringValue)
            self.performSegue(withIdentifier: "showMain", sender: self)
        }
    }
    
    @IBAction func didTouchSkip(_ sender: UIBarButtonItem) {
        
    }
    
}

//
//  SignUpVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import PhoneNumberKit
import SwiftValidators
import SwiftyJSON

class SignUpVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var companyTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: PhoneNumberTextField!
    @IBOutlet weak var stationTextField: UITextField!
    
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var signInButton: UIButton!
    
    let stationsPicker = UIPickerView()
    
    var stations: [JSON] = [JSON]()
    var currentStations: [JSON] = [JSON]()
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getStations()
        
        registerForKeyboardNotifications()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
        nameTextField.layer.borderColor = UIColor.lightGray.cgColor
        nameTextField.layer.borderWidth = 1
        nameTextField.layer.cornerRadius = 2
        nameTextField.setLeftPaddingPoints(16)
        nameTextField.setRightPaddingPoints(16)
        companyTextField.layer.borderColor = UIColor.lightGray.cgColor
        companyTextField.layer.borderWidth = 1
        companyTextField.layer.cornerRadius = 2
        companyTextField.setLeftPaddingPoints(16)
        companyTextField.setRightPaddingPoints(16)
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 2
        emailTextField.setLeftPaddingPoints(16)
        emailTextField.setRightPaddingPoints(16)
        phoneNumberTextField.layer.borderColor = UIColor.lightGray.cgColor
        phoneNumberTextField.layer.borderWidth = 1
        phoneNumberTextField.layer.cornerRadius = 2
        phoneNumberTextField.setLeftPaddingPoints(16)
        phoneNumberTextField.setRightPaddingPoints(16)
        stationTextField.layer.borderColor = UIColor.lightGray.cgColor
        stationTextField.layer.borderWidth = 1
        stationTextField.layer.cornerRadius = 2
        stationTextField.setLeftPaddingPoints(16)
        stationTextField.setRightPaddingPoints(16)
        stationTextField.inputView = stationsPicker
        stationsPicker.delegate = self
        stationsPicker.dataSource = self
        setupStationsDatePicker()
        
        signInButton.layer.borderColor = UIColor.lightGray.cgColor
        signInButton.layer.borderWidth = 1
        signInButton.layer.cornerRadius = 2
        
        signUpButton.layer.cornerRadius = 2
    }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    // MARK: - Keyboard
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    private func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView.contentInset.bottom = keyboardSize.height - (UIScreen.main.bounds.height - signInButton.frame.origin.y - signInButton.frame.height) + 36
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
    
    func getStations() {
        APIManager.shared.getStations(page: 1) { (json, error) in
            if let error = error {
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stations = json!["data"].arrayValue
            self.selectStation = self.stations.first
            
            APIManager.shared.getStations(page: 2) { (json, error) in
                if let error = error {
                    showAlert(self, title: error, message: nil)
                    return
                }
                
                self.stations += json!["data"].arrayValue
            }
        }
    }
    
    // MARK: - Setup Date Picker
    
    var selectStation: JSON? = nil
    private func setupStationsDatePicker() {
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.tintColor = .blue
        toolBar.sizeToFit()
        
        let deleteButton = UIBarButtonItem(title: "Очистить", style: .plain, target: self, action: #selector(deleteStations))
        let doneButton = UIBarButtonItem(title: "Добавить", style: .plain, target: self, action: #selector(addStation))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.setItems([deleteButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        stationTextField.inputAccessoryView = toolBar
        stationTextField.inputView = stationsPicker
    }
    
    @objc func addStation() {
        if let selectStation = selectStation {
            currentStations.append(selectStation)
            if stationTextField.text == "" {
                stationTextField.text = selectStation["name"].stringValue
            } else {
                stationTextField.text = stationTextField.text! + ", \(selectStation["name"].stringValue)"
            }
            dismissKeyboard()
        }
    }
    
    @objc func deleteStations() {
        currentStations.removeAll()
        stationTextField.text = ""
        dismissKeyboard()
    }
    
    // MARK: - Segua
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showAlert" {
            let alertVC: AlertSendCodeVC = segue.destination as! AlertSendCodeVC
            alertVC.isSignUp = true
            alertVC.signUpVC = self
        }
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchSignUp(_ sender: UIButton) {
        dismissKeyboard()
        
        if Validator.isEmpty().apply(nameTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите ФИО", message: nil)
            return
        }
        
        if !Validator.isEmail().apply(emailTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите email", message: nil)
            return
        }
        
        if Validator.isEmpty().apply(companyTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите свою компанию", message: nil)
            return
        }

        if !phoneNumberTextField.isValidNumber {
            showAlert(self, title: "Введите номер телефона", message: nil)
            return
        }
        
        if Validator.isEmpty().apply(stationTextField.text?.trimmingTrailingSpaces()) || currentStations.isEmpty {
            showAlert(self, title: "Введите станцию", message: nil)
            return
        }
        
        startLoading()
        var idStations = [Int]()
        for station in currentStations {
            print(station)
            idStations.append(Int(station["station_id"].stringValue) ?? 0)
        }
        APIManager.shared.signUp(fullname: nameTextField.text!.trimmingTrailingSpaces(), email: emailTextField.text!.trimmingTrailingSpaces(), organization: companyTextField.text!.trimmingTrailingSpaces(), phone: phoneNumberTextField.text!, stationId: idStations) { (json, error) in
            if let error = error {
                self.stopLoading()
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stopLoading()
            self.performSegue(withIdentifier: "showAlert", sender: self)
//            let alert = UIAlertController(title: "Ваш запрос принят. Код активации устройства будет выслан на ваш email.", message: json!["msg"].stringValue, preferredStyle: .alert)
//            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
//                self.navigationController?.popViewController(animated: true)
//            }))
//            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func didTouchSignIn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension SignUpVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return stations.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return stations[row]["name"].stringValue
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectStation = stations[row]
    }
}

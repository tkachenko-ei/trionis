//
//  AlertSendCodeVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit

class AlertSendCodeVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var isSignUp = false
    var signUpVC: SignUpVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isSignUp {
            titleLabel.text = "Ваш запрос принят. Код активации устройства будет выслан на ваш email."
        }
    }
    
    @IBAction func didTouchOk(_ sender: UIButton) {
        dismiss(animated: true, completion: {
            if self.isSignUp {
                self.signUpVC?.performSegue(withIdentifier: "showMain", sender: self.signUpVC)
            }
        })
    }
    
}

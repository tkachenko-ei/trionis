//
//  RestoreAccessVC.swift
//  Trionis
//
//  Created by Evgeniy on 30.06.2018.
//  Copyright © 2018 Trionis. All rights reserved.
//

import UIKit
import SwiftValidators

class RestoreAccessVC: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var getCodeButton: UIButton!
    
    // MARK: - Init
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registerForKeyboardNotifications()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        
        emailTextField.layer.borderColor = UIColor.lightGray.cgColor
        emailTextField.layer.borderWidth = 1
        emailTextField.layer.cornerRadius = 2
        
        emailTextField.setLeftPaddingPoints(16)
        emailTextField.setRightPaddingPoints(16)
        
        getCodeButton.layer.cornerRadius = 2
    }
    
    deinit {
        removeKeyboardNotifications()
    }
    
    // MARK: - Keyboard
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(_:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(_:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    private func removeKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc private func keyboardWillShow(_ notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            scrollView.contentInset.bottom = keyboardSize.height - (UIScreen.main.bounds.height - getCodeButton.frame.origin.y - getCodeButton.frame.height) + 36
        }
    }
    
    @objc private func keyboardWillHide(_ notification: Notification) {
        scrollView.contentInset.bottom = 0
    }
    
    // MARK: - Actions
    
    @IBAction func didTouchGetCode(_ sender: UIButton) {
        dismissKeyboard()
        
        if !Validator.isEmail().apply(emailTextField.text?.trimmingTrailingSpaces()) {
            showAlert(self, title: "Введите email", message: nil)
            return
        }
        
        startLoading()
        APIManager.shared.restorePassword(email: emailTextField.text!.trimmingTrailingSpaces()) { (error) in
            if let error = error {
                self.stopLoading()
                showAlert(self, title: error, message: nil)
                return
            }
            
            self.stopLoading()
            self.performSegue(withIdentifier: "showAlert", sender: self)
        }
    }
    
    @IBAction func didTouchSignIn(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}
